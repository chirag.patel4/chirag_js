let Ids = new Array();
let itemList = new Array();
let completedItemList = new Array();
let counter = 1;

function addToDoItem() {
    let ul = document.getElementById("dynamic-list");
    let iItem = document.getElementById("iItem");
    let li = document.createElement("li");
    li.setAttribute('id', counter);
    li.addEventListener("dblclick", myScript);
    let multiIds = new Array();
    Ids.push([iItem.value, false]);
    itemList.push(iItem.value);
    li.appendChild(document.createTextNode(iItem.value));
    ul.appendChild(li);
    counter++;
    document.getElementById("clearListItems").disabled = false;
    document.getElementById("save").disabled = false;
    document.getElementById("addItem").disabled = true;
    iItem.value = null;
}

function myScript(event) {
    let selectedItem = event.target;
    const text = selectedItem.id;
    if (selectedItem.classList != null && selectedItem.classList != undefined &&
        selectedItem.classList.contains('completed')) {
        completedItemList.pop(text);
        selectedItem.classList.remove('completed');
        // Ids[selectedItem.id +1] = false;
        
        if (Ids.length == 0) {
            document.getElementById("clearCompleted").disabled = true;
        }
    } else {
        completedItemList.push(text);
        selectedItem.classList.add('completed');
        // Ids[selectedItem.id +1] = true;
        // alert(Ids[selectedItem.id]);
        if (Ids.length != 0) {
            document.getElementById("clearCompleted").disabled = false;
        }
    }
}


function removecompletedItemList() {
    completedItemList.forEach(str => {
        let element = document.getElementById(str);
        if (element != null && element != undefined &&
            element.classList != null && element.classList != undefined &&
            element.classList.contains('completed')) {
            element.parentNode.removeChild(element);
            Ids.pop(str[0]);
            itemList.pop(element.innerText)
        }
    });
    completedItemList = new Array();
    document.getElementById("clearCompleted").disabled = true;
    if (Ids.length = 0) {
        document.getElementById("clearListItems").disabled = true;
    }
}

function clearListItems() {
    Ids.forEach(str => {
        let element = document.getElementById(str[0]);
        element.parentNode.removeChild(element);
    });
    Ids = new Array();
    itemList = new Array();
    completedItemList = new Array();
    document.getElementById("clearCompleted").disabled = true;
    document.getElementById("clearListItems").disabled = true;
    document.getElementById("save").disabled = true;
}

function saveList() {
    if (Ids.length != 0) {
        let x = document.getElementById("second_div");
        x.style.display = "block";
    }
    Ids.forEach(str => {
        let ul = document.getElementById("dynamic-list2");
        // let iItem = document.getElementById("dynamic-list");
        let li = document.createElement("li");
        let mergedItem = str[0] + ' - ' + str[1];
        li.appendChild(document.createTextNode(mergedItem));
        ul.appendChild(li);
    });
}

function onBlur() {
    let iItem = document.getElementById("iItem");
    if (iItem.value != null && iItem.value != undefined && iItem.value != '') {
        document.getElementById("addItem").disabled = false;
    } else {
        document.getElementById("addItem").disabled = true;
    }
}
